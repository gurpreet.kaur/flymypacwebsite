

import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import {Provider} from 'react-redux';
import store from "../src/store/store";
import "./assets/css/anshu.css";
import "./assets/css/flymypac.css";
import "./assets/css/style.css";
import AuthLayout from "./layouts/Auth";
import MainLayout from "./layouts/Main";
import AxiosConfig from "./utils/responseHandling/AxiosConfig";

AxiosConfig();
const hist = createBrowserHistory();

ReactDOM.render(
  <Provider store={store}>
  <Router history={hist}>
    <Switch>
    <Route exact path="/">
          {<Redirect to="/auth/login" />}
        </Route>
      <Route path="/auth" render={(props) => <AuthLayout {...props} />} />
       <Route path="/main" render={(props) => <MainLayout {...props} />} />
    </Switch>
  </Router>
   </Provider>
  ,
  document.getElementById("root")
);
