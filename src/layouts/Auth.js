
import React from "react";
// javascript plugin used to create scrollbars on windows
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";
import routes from "../routes";

var ps;

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: "black",
      activeColor: "info",
    };
    this.mainPanel = React.createRef();
  }
  componentDidMount() {
    //  if (this.props.userRegister.authState) {
      
    //    this.props.history.push("/main/Main");
    //  }
  }
  componentDidUpdate(e) {
    //  if (this.props.userRegister.authState) {
    //   this.props.history.push("/main/Main");
    //  }
  }

  handleActiveClick = (color) => {
    this.setState({ activeColor: color });
  };
  handleBgClick = (color) => {
    this.setState({ backgroundColor: color });
  };
  render() {
    return (
      <div >
      
        <div >
          
         { <Switch>
            {routes.map((prop, key) => {
                if (prop.protected === "auth") {
              return (
                <Route
                {...this.props}
                  path={prop.layout + prop.path}
                  component={prop.component}
                  key={key}
                />
              );
                }
            })}
          </Switch>}
      
        </div>
      
      </div>
    );
  }
}

const mapStateToProps = (state) => ({ userRegister: state.userRegister });
export default connect(mapStateToProps)(Login);
