
import React from "react";
import { connect } from "react-redux";
// javascript plugin used to create scrollbars on windows
import { Route, Switch } from "react-router-dom";

import routes from "../routes.js";

var ps;

class Main extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgroundColor: "black",
      activeColor: "info",
    };
    this.mainPanel = React.createRef();
  }
  componentDidMount() {
console.log(this.props.userRegister.authState)
    if (!this.props.userRegister.authState) {
      
      this.props.history.push("/auth/login");
    }

  }  
  componentDidUpdate(e) {
    console.log(this.props.userRegister)
    if (!this.props.userRegister.authState) {
      this.props.history.push("/auth/login");
    }
  }

  componentWillUnmount() {
    console.log(this.props.userRegister)
    if (!this.props.userRegister.authState) {
      this.props.history.push("/auth/login");
    }

  }
  
 
  render() {
    return (
      <div >
        <div >
          
         { <Switch>
            {routes.map((prop, key) => {
                if(prop.protected === 'main'){
                    return (
                        <Route
                          {...this.props}
                          path={prop.layout + prop.path}
                          component={prop.component}
                          key={key}
                        />
                      );
                }
              
            })}
          </Switch>}
      
        </div>
      
      </div>
    );
  }
}
const mapStateToProps = (state) => ({ userRegister: state.userRegister });
export default connect(mapStateToProps)(Main);

