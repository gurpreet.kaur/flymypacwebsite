import {
  USER_NAME_LOADING,
  USER_NAME_SUCCESS,
  USER_NAME_ERROR,

  USER_NUMBER_LOADING,
  USER_NUMBER_SUCCESS,
  USER_NUMBER_ERROR , 
  SAVE_NUMBER_LOADING,

  USER_REGISTER_LOADING,
  USER_REGISTER_SUCCESS,
  USER_REGISTER_ERROR,
  
  USER_DOCUMENT_LOADING,
  USER_DOCUMENT_SUCCESS,
  USER_DOCUMENT_ERROR,

  USER_BANK_LOADING,
  USER_BANK_SUCCESS,
  USER_BANK_ERROR,

  USER_SIGNIN_LOADING,
  USER_LOGOUT,
  USER_SIGNIN_ERROR,
  USER_SIGNIN_SUCCESS,
} from "../utils/constants/userConstants";

function userNameReducer(state = { loading: true , userName: '' }, action) {
  switch (action.type) {
    case USER_NAME_LOADING:
      return { loading: true };
    case USER_NAME_SUCCESS:
      return { loading: false, userName: action.payload };
    case USER_NAME_ERROR:
      return {
        loading: false,
      };
   
    default:
      return state;
  }
}

function verifyNumberReducer(state = { loading: true , otp: '' }, action) {
  switch (action.type) {
    case USER_NUMBER_LOADING:
      return { loading: true };
    case USER_NUMBER_SUCCESS:
      return { loading: false, otp: action.payload };
    case USER_NUMBER_ERROR:
      return {
        loading: false,
      };
   
    default:
      return state;
  }
}
function saveNumberReducer(state = {  details: [] }, action) {
  switch (action.type) {
    case SAVE_NUMBER_LOADING:
      return { loading: false, details: action.payload }
    default:
      return state;
  }
}

function userBankReducer(state = { loading: true , bankdetails: [] }, action) {
  switch (action.type) {
    case USER_BANK_LOADING:
      return { loading: true };
    case USER_BANK_SUCCESS:
      return { loading: false, bankdetails: action.payload };
    case USER_BANK_ERROR:
      return {
        loading: false,
      };
   
    default:
      return state;
  }
}

function userRegisterReducer(state = { loading: true,  }, action) {
  switch (action.type) {
    case USER_REGISTER_LOADING:
      return { loading: true };
    case USER_REGISTER_SUCCESS:
      return { loading: false, authState: action.payload };
    case USER_REGISTER_ERROR:
      return {
        loading: false,
      };
  
    default:
      return state;
  }
}

function userDocumentsReducer(state = { loading: true, document:[] }, action) {
  switch (action.type) {
    case USER_DOCUMENT_LOADING:
      return { loading: true };
    case USER_DOCUMENT_SUCCESS:
      return { loading: false, document: action.payload };
    case USER_DOCUMENT_ERROR:
      return {
        loading: false,
      };
  
    default:
      return state;
  }
}


function userSigninReducer(state = { loading: true }, action) {
  switch (action.type) {
    case USER_SIGNIN_LOADING:
      return { loading: true };
    case USER_SIGNIN_SUCCESS:
      return { loading: false, authState: action.payload };
    case USER_SIGNIN_ERROR:
      return {
        loading: false,
      };
    case USER_LOGOUT:
      return { loading: false, authState: "" };
    default:
      return state;
  }
}

export { userNameReducer, verifyNumberReducer,saveNumberReducer,userBankReducer,userRegisterReducer,userDocumentsReducer,userSigninReducer  };
