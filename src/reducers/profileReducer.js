import {
  PROFILE_DATA_SUCCESS,
  PROFILE_DATA_LOADING,
  PROFILE_DATA_ERROR,
  UPDATE_PROFILE_DATA_SUCCESS,
  UPDATE_PROFILE_DATA_LOADING,
  UPDATE_PROFILE_DATA_ERROR,
} from "utils/constants/profileConstants";

function profileReducer(state = { loading: true, profile: [] }, action) {
  switch (action.type) {
    case PROFILE_DATA_LOADING:
      return { loading: true, profile: [] };
    case PROFILE_DATA_SUCCESS:
      return { loading: false, profile: action.payload };
    case PROFILE_DATA_ERROR:
      return {
        loading: false,
      };
    default:
      return state;
  }
}

function UpdateprofileReducer(state = { loading: true, Updateprofile: [] }, action) {
  switch (action.type) {
    case UPDATE_PROFILE_DATA_LOADING:
      return { loading: true, Updateprofile: [] };
    case UPDATE_PROFILE_DATA_SUCCESS:
      return { loading: false, Updateprofile: action.payload };
    case UPDATE_PROFILE_DATA_ERROR:
      return {
        loading: false,
      };
    default:
      return state;
  }
}

export { profileReducer,UpdateprofileReducer };
