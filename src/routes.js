
import Login from "./views/auth/Login";
import SignUp from "./views/auth/SignUp";
import Verification from "./views/auth/Verification";
import AccountDetail from "./views/auth/AccountDetail";
import TermsAndConditions from "./views/auth/TermsAndConditions";
import UploadLicence from "./views/auth/UploadLicence";
import Home from "./views/main/Home";
import Profile from "./views/main/Profile";


//testuser@mail.com
//1234

var routes = [
  {
    path: "/login",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: Login,
    layout: "/auth",
     protected: 'auth',
  },
  {
    path: "/signup",
    name: "Dashboard",
    icon: "nc-icon nc-bank",
    component: SignUp,
    layout: "/auth",
     protected: 'auth',
  },
  {
    path: "/verification",
    name: "verification",
    icon: "nc-icon nc-bank",
    component: Verification,
    layout: "/auth",
     protected: 'auth',
  },
  {
    path: "/accountdetail",
    name: "accountdetail",
    icon: "nc-icon nc-bank",
    component: AccountDetail,
    layout: "/auth",
     protected: 'auth',
  },
  {
    path: "/termsandconditions",
    name: "termsandconditions",
    icon: "nc-icon nc-bank",
    component: TermsAndConditions,
    layout: "/auth",
     protected: 'auth',
  },
  {
    path: "/uploadlicence",
    name: "uploadlicence",
    icon: "nc-icon nc-bank",
    component: UploadLicence,
    layout: "/auth",
     protected: 'auth',
  },
  {
    path: "/home",
    name: "home",
    icon: "nc-icon nc-bank",
    component: Home,
    layout: "/auth",
     protected: 'auth',
  },
  {
    path: "/profile",
    name: "profile",
    icon: "nc-icon nc-bank",
    component: Profile,
    layout: "/auth",
     protected: 'auth',
  },
  
];
export default routes;
