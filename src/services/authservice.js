import axios from "axios";
import LocalStorageService from "../services/localstorageService";
import {
  USER_NAME_LOADING,
  USER_NAME_SUCCESS,
  USER_NAME_ERROR,

  USER_NUMBER_LOADING,
  USER_NUMBER_SUCCESS,
  USER_NUMBER_ERROR , 
  SAVE_NUMBER_LOADING , 

  USER_BANK_LOADING,
  USER_BANK_SUCCESS,
  USER_BANK_ERROR,

USER_REGISTER_LOADING,
USER_REGISTER_SUCCESS,
USER_REGISTER_ERROR,

USER_DOCUMENT_LOADING,
USER_DOCUMENT_SUCCESS,
USER_DOCUMENT_ERROR,

USER_SIGNIN_LOADING,
USER_SIGNIN_SUCCESS,
USER_SIGNIN_ERROR,
USER_LOGOUT,

} from "../utils/constants/userConstants";
import { env } from "../environment/environment";

const userName = (firstName) => async (dispatch) => {
  dispatch({
    type: USER_NAME_LOADING,
    payload: firstName,
  });
  try {
    const  data  = await axios.post(`${env}user/userName`,{
      firstName
    }, );
    dispatch({ type: USER_NAME_SUCCESS, payload: data.data.data });
    console.log('data--->',data.data.data);
  } catch (error) {
    dispatch({ type: USER_NAME_ERROR, payload: error.response.data.message });
  }
};

const verifyNumber = (phone,countryCode) => async (dispatch) => {
  dispatch({
    type: USER_NUMBER_LOADING,
    payload: phone,
  });
  try {
    const  data  = await axios.post(`${env}user/sentOtp`,{
      phone,
      countryCode,
    }, );
    dispatch({ type: USER_NUMBER_SUCCESS, payload: data.data.data });
    console.log('data--->',data.data.data);
  } catch (error) {
    dispatch({ type: USER_NUMBER_ERROR, payload: error.response.data.message });
  }
};

const saveNumber = (details) => async (dispatch) => {
  dispatch({
    type: SAVE_NUMBER_LOADING,
    payload: details,
  });

};

const userBank = (accessToken, bankName,accountHolderName,accountNumber,accountType ) => async (dispatch) => {
  dispatch({
    type: USER_BANK_LOADING,
    payload: '',
  });
  try {
    const  data  = await axios.post(`${env}user/bank`,{
      bankName,
      accountHolderName,
      accountNumber, 
      accountType
    }, { headers: {
      Authorization: accessToken,
    }});
    dispatch({ type: USER_BANK_SUCCESS, payload: data.data.data });
    console.log('bank data--->',data);
  } catch (error) {
    dispatch({ type: USER_BANK_ERROR, payload: error.response.data.message });
  }
};

const userRegister = (userdata) => async (dispatch) => {
  dispatch({
    type: USER_REGISTER_LOADING,
    payload: '',
  });
  try {
    const  data  = await axios.post(`${env}user/register`, userdata );
    dispatch({ type: USER_REGISTER_SUCCESS, payload: data.data.data.token });
    console.log('USER data:---- --->',data.data.data.token);
    LocalStorageService._setAccessToken(data.data.data.token);
  } catch (error) {
    dispatch({ type: USER_REGISTER_ERROR, payload: error.response.data.message });
  }
};

const userDocuments = (accessToken,userdocuments) => async (dispatch) => {
  dispatch({
    type: USER_DOCUMENT_LOADING,
    payload: '',
  });
  try {
    const  data  = await axios.post(`${env}user/documents`, 
     userdocuments
    ,{ headers: {
      Authorization: accessToken,
    }}
     );
    dispatch({ type: USER_DOCUMENT_SUCCESS, payload: data });
   
  } catch (error) {
    dispatch({ type: USER_DOCUMENT_ERROR, payload: error.response.data.message });
  }
};

// const logout = () => async (dispatch) => {
//   LocalStorageService.clearToken();
//   dispatch({ type: USER_LOGOUT });
// };


const signin = (email, password) => async (dispatch) => {
  dispatch({
    type: USER_SIGNIN_LOADING,
    payload: { email, password },
  });
  try {
    const { data } = await axios.post(`${env}user/login`, {
      userName,
      password,
    });
    dispatch({ type: USER_SIGNIN_SUCCESS, payload: data.data.accessToken });

    LocalStorageService._setAccessToken(data.data.accessToken);
  } catch (error) {
    dispatch({ type: USER_SIGNIN_ERROR, payload: error.response.data.message });
  }
};


export { userName ,verifyNumber,saveNumber,userBank ,userRegister,userDocuments };
