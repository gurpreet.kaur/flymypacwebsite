import axios from "axios";
import {
  PROFILE_DATA_LOADING,
  PROFILE_DATA_SUCCESS,
  PROFILE_DATA_ERROR,
  UPDATE_PROFILE_DATA_LOADING,
  UPDATE_PROFILE_DATA_SUCCESS,
  UPDATE_PROFILE_DATA_ERROR,
} from "../utils/constants/profileConstants";
import { env } from "../environment/environment";

const getProfileData = (accessToken) => async (dispatch) => {
  dispatch({
    type: PROFILE_DATA_LOADING,
  });
  try {
    const response = await axios.post(`${env}/admin/getProfile`, {
      headers: {
        Authorization: accessToken,
      },
    });

   // console.log("response-->", response.data.data);
    dispatch({ type: PROFILE_DATA_SUCCESS, payload: response.data.data });
  } catch (error) {
    //console.log("response", error);
    dispatch({
      type: PROFILE_DATA_ERROR,
      payload: error.response,
    });
  }
};

const updateAdminProfileData = (accessToken,firstName) => async (dispatch) => {
  dispatch({
    type: UPDATE_PROFILE_DATA_LOADING,
  });
  try {
    const response = await axios.post(`${env}/admin/updateAdminProfile`,{
      firstName
    }, {
      headers: {
        Authorization: accessToken,
      },
    });

   // console.log("updated data", response);
  //  dispatch({ type: UPDATE_PROFILE_DATA_SUCCESS, payload: response.data.data });
  } catch (error) {
    //console.log("response", error);
    dispatch({
      type: UPDATE_PROFILE_DATA_ERROR,
      payload: error.response,
    });
  }
};

export { getProfileData,updateAdminProfileData };
