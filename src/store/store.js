import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import {  userNameReducer,
  saveNumberReducer,verifyNumberReducer,
  userBankReducer,userRegisterReducer,
  userDocumentsReducer,userSigninReducer  } from '../reducers/authReducer';
const authState = localStorage.getItem("access_token");
 const initialState = {
   userSignin: { authState }, 
 };
const reducer = combineReducers({
  userName: userNameReducer,
  userNumber: saveNumberReducer,
  userOtp: verifyNumberReducer,
  userBank: userBankReducer,
  userRegister: userRegisterReducer,
  userDocument: userDocumentsReducer,
  userSignin: userSigninReducer,
});
//window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducer,
  initialState,
  composeEnhancer(applyMiddleware(thunk))
);
export default store;
