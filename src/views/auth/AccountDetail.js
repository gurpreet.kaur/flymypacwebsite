import React,{useState,useEffect} from "react";
import { userBank} from '../../services/authservice';
import {useDispatch,useSelector } from 'react-redux';
import {useParams,useLocation} from 'react-router-dom';
import { Link,useHistory } from "react-router-dom";

const AccountDetail = () => {

        const dispatch = useDispatch();
        const history = useHistory();
        const { authState } = useSelector((state) => state.userRegister);
        const[accountType,setAccountType] = useState('');
       const [bankname, setBank] = useState("");
       const [accountholder, setAccountholder] = useState("");
       const [accountnumber, setAccountNumber] = useState("");

       useEffect(() => {
     
        console.log("user token", authState);
       
       }, [authState]);
   

     const VerifyDetails = (e) => {
         e.preventDefault();
         dispatch(userBank(authState,bankname,accountholder,accountnumber,accountType))
         history.push('/auth/termsandconditions', {
            number: 'ghghg'  });
     }


    

    return (
        <div class="wrap">
        <div class="choose-option">
            <div class="co-left">
                <figure class="content-img">
                    <img src="../assets/images/png/verification.png"/>
                </figure>
                <div class="back-btn">
                    <figure class="backicon">
                      <a href="index.html"><img src="../assets/images/svg/back.svg"/></a> 
                    </figure>
                </div>
            </div>
            <div class="co-right justify-content-start pt-5 pb-5">
                <figure class="logo">
                    <img src="../assets/images/svg/logo.svg"/>
                </figure>
                <form class="auth-form pb-0"  onSubmit={VerifyDetails}  >
                    <button class="btn btn-warning">Paypal</button>
                    <p class="authorise-para text-center mb-0 pt-2 pb-2">Or</p>
                    <ul>
                        <p class="authorise-para mb-0">Bank Account</p>
                        <li>
                            <input type="text" 
                             class="formcontrol"
                             placeholder="Bank Name"
                             value={bankname}
                             onChange={(e) => {
                               setBank(e.target.value)
                             }}
                             required
                             />
                        </li>
                        <li>
                            <input type="text" 
                            class="formcontrol" 
                            placeholder="Account Holder Name"
                            value={accountholder}
                            onChange={(e) => {
                                setAccountholder(e.target.value)
                            }}
                            required
                            />
                        </li>
                        <li>
                            <input type="text"
                             class="formcontrol"
                             placeholder="Account Number"
                             value={accountnumber}
                             onChange={(e) => {
                                setAccountNumber(e.target.value)
                             }}
                             required
                              />
                        </li>
                        <li>
                          
                            <div class="input-layout ">
                            <select 
                               name=""
                               id="dropdown"
                               onChange={(e) => {
                                 setAccountType(e.target.value);
                               }}
                            >
                            <option  disabled selected hidden>Account Type</option>
                            <option value="Current" >Current</option>
                            <option  value="Saving"  >Saving</option>
                            </select>
                            </div>
                        </li>
                     </ul>
              
                <div class="termC pt-0 pb-3">
                    <div class="d-flex termCinner">
                        <div class="terms-input"><input type="checkbox" id="" name="" value="" data-toggle="modal"
                                data-target="#conditions"/></div>
                        <div><label for="" class="text-para mb-0">I understand that payment will proceed upon confirmation of delivery</label></div>
                    </div>
                </div>
                <button class="btn btn-primary" onSubmit={(e) => VerifyDetails(e) } >
                    Authorize</button>
                </form>
            </div>
            
        </div>
    </div>
    );
  
}

export default AccountDetail;
//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDlhMjIxMzEwNDJiMjBmYzEzMzQ3MWEiLCJpYXQiOjE2MjA3MTQwMDMsImV4cCI6MTYyMTMxODgwM30.OQnUVUnIWdeCBXTbAyjwGSCLhH1-G4yR4Xspd_FLkVY