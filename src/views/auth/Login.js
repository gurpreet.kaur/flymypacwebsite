
import React,{useState, useEffect} from "react";
import { Link,useHistory } from "react-router-dom";
import { signin } from "../../services/signinService";
import { useSelector, useDispatch } from "react-redux";

const Login = () => {
  const history = useHistory();
  const [username, setusername] = useState("");
  const [password, setPassword] = useState("");
  const dispatch = useDispatch();
  const userSignin = useSelector((state) => state.userSignin);

  const onLoginRequest = (e) => {     
    e.preventDefault();
    dispatch(signin(username, password));
    history.push('/auth/home', {
      number: 'ghghg'  });
  };

    return (
      <>
        <div class="wrap">
        <div class="choose-option">
            <div class="co-left">
                <figure class="side-banner">
                    <img src="../assets/images/jpg/login.jpg"/>
                </figure>
                <div class="back-btn">
                    <figure class="backicon">
                      <a href="index.html"><img src="../assets/images/svg/back.svg"/></a> 
                    </figure>
                </div>
            </div>
            <div class="co-right">
                <figure class="logo">
                    <img src="../assets/images/svg/logo.svg"/>
                </figure>
               <form class="auth-form" onSubmit={onLoginRequest} >
                   <ul>
                       <li>
                           <input type ="text" 
                           class="formcontrol" 
                           placeholder="Email or Phone"
                           onChange={(e) => {
                            setusername(e.target.value);
                          }}
                          required
                           />
                       </li>
                       <li class="mb-0">
                        <input type="password" 
                        class="formcontrol"
                         placeholder="Password"
                         onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                        required
                         />
                    </li>
                   </ul>
              
                <button class="btn btn-primary" 
                onSubmit={(e) => onLoginRequest(e) }
                >Login</button>
                 </form>
                <div class="oroption">
                    <p class="text-para">Don’t have an account? 
                    { <Link to="/auth/Signup">Sign Up</Link>} 
                    </p>
                 </div>
            </div>
        </div>
    </div>
      </>
    );
  }


export default Login;
