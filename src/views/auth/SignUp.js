import React,{useState,useEffect} from "react";
import { userName,saveNumber,verifyNumber} from '../../services/authservice';
import {useDispatch,useSelector } from 'react-redux';
import { Link,useHistory } from "react-router-dom";


const SignUp = () => {
  const dispatch = useDispatch();
  const _userName = useSelector((state) => state.userName);

  const history = useHistory();
    const [file,setFile] = useState('');
    const [filename,setFileName] = useState('');
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [username, setUsername] = useState("");
    const [firstname, setFirstname] = useState("");
    const [lastname, setLastname] = useState("");
    const [middlename, setMiddlename] = useState("");
    const [phonenumber, setPhonenumber] = useState("");
    const [terms, setTerms] = useState("");
    const [notifications, setNotifications] = useState("");
    
    const code = '+91'

     const VerifyDetails = (e) => {
      e.preventDefault();

      let formData = new FormData();
      formData.append('email', email);
      formData.append('firstName', firstname);
      formData.append('middleName',middlename)
      formData.append('lastName', lastname);
      formData.append('userName', username);
      formData.append('phone', phonenumber);
      formData.append('countryCode', code);
      formData.append('password', password);
      formData.append('longitude', '31.434534534');
      formData.append('latitude', '75.234234234');
      formData.append('customerId', username);
      formData.append('profile', file);
      // console.log('formdata',formData)
       //for (var key of formData.entries()) {
      //  console.log(key[0] + ", " + key[1]);
     // }
     
       
       
       dispatch(saveNumber(formData));
       dispatch(verifyNumber(phonenumber,code))
      history.push('/auth/verification', {
          number: 'ghghg'  });
        
     }

     const setFileUpload = (e) => {
     setFile(e.target.files[0]);
     setFileName(e.target.files[0].name);
  
     }

     useEffect(() => {
     
     console.log("fetched user name", _userName.userName);
     setUsername(_userName.userName)
    }, [_userName]);

    

     useEffect(() => {
      const delayDebounceFn = setTimeout(() => {
        console.log( 'name', firstname)
        // Send Axios request here
        if(firstname){
          dispatch(userName(firstname));
        }
        
      }, 3000)
  
      return () => clearTimeout(delayDebounceFn)
    }, [firstname])


  
    return (
      <>
        <div class="wrap">
          <div class="choose-option">
            <div class="co-left">
              <figure class="side-banner">
                <img src="../assets/images/jpg/login.jpg" />
              </figure>
            </div>
          
            <div class="co-right justify-content-start pt-5">
           
              <div class="upload-img">
              
                <div  style={{backgroundColor:' red'}}>
                  <input type="file"
                   id="profile"
                   name='profile'
                    onChange={(e) => setFileUpload(e)}
                    />
                 {/* {  <button onClick={() => onFileUpload()}   >select </button>} */}
                </div>
              </div>
              <form class="auth-form pb-0" 
              onSubmit={VerifyDetails}
              >
                <ul>
                  <li>
                    <input
                      type="text"
                      class="formcontrol"
                      placeholder="First Name"
                      value={firstname}
                      onChange={(e) => {
                        setFirstname(e.target.value)
                      }}
                      required
                      
                    />
                  </li>
                  <li>
                    <input
                      type="text"
                      class="formcontrol"
                      placeholder="Middle Name"
                      value={middlename}
                      onChange={(e) => {
                        setMiddlename(e.target.value)
                        
                      }}
                      required
                    />
                  </li>
                  <li>
                    <input
                      type="text"
                      class="formcontrol"
                      placeholder="Last Name"
                      value={lastname}
                      onChange={(e) => {
                        setLastname(e.target.value);
                      }}
                      required
                    />
                  </li>
                  <li>
                    <input
                      type="email"
                      class="formcontrol"
                      placeholder="Email ID"
                      pattern="^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$"
                      value={email}
                      onChange={(e) => {
                        setEmail(e.target.value);
                      }}
                      required
                    />
                  </li>
                  <li>
                    <input
                      type="text"
                      class="formcontrol"
                      placeholder="+91 Phone Number"
                      value={phonenumber}
                      onChange={(e) => {
                        setPhonenumber(e.target.value);
                      }}
                      required
                    />
                  </li>
                  <li>
                    <input
                      type="text"
                      disabled={true}
                      class="formcontrol"
                      placeholder="Username"
                     defaultValue={username}
                      value={username}
                    />
                  </li>
                  <li>
                    <input
                      type="password"
                      class="formcontrol"
                      placeholder="Password"
                      onChange={(e) => {
                        setPassword(e.target.value);
                      }}
                      value={password}
                      required
                    />
                  </li>
                  <li class="mb-0">
                    <input
                      type="password"
                      class="formcontrol"
                      placeholder="Confirm Password"
                      value={confirmPassword}
                      onChange={(e) => {
                        setConfirmPassword(e.target.value);
                      }}
                      required
                    />
                    
                    {confirmPassword.length !=0 &&  confirmPassword != password ?
                      <text> passwords do not match</text> :
                      null
                      }
                  </li>
                </ul>
              
              <div class="termC">
                <div class="d-flex termCinner">
                  <div class="terms-input">
                    <input
                      type="checkbox"
                      id=""
                      name=""
                      data-toggle="modal"
                      data-target="#conditions"
                      
                    />
                  </div>
                  <div>
                    <label for="" class="text-para mb-0">
                      I agree to the Terms and Privacy Policy
                    </label>
                  </div>
                </div>
                <div class="d-flex termCinner mb-0">
                  <div class="terms-input">
                    <input type="checkbox"
                     id=""
                     name=""
                     //value={notifications} 
                    // onChange={(e) => {setNotifications(e.target.value);}}
                     />
                  </div>
                  <label for="" class="text-para mb-0">
                    I would like to receive email notifications
                  </label>
                 
                </div>
              </div>
              <button class="btn btn-primary" 
              onSubmit={(e) =>  VerifyDetails(e) }
              > Continue
              </button>
              </form>
              <div class="oroption">
                <p class="text-para">
                  Already have an account <Link to="/auth/login">Login</Link>
                </p>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  
}

export default SignUp;
 {/* <Link to={{
                                 pathname: "/auth/verification",
                                // search: "?sort=name",
                                 //hash: "#the-hash",
                                 state: { number: phonenumber  }
                                  }}>Continue</Link> */}