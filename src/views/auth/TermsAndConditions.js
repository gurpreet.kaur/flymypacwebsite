import React, { useState, useEffect } from "react";
import { userName,saveNumber,userRegister} from '../../services/authservice';
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useParams, useHistory } from "react-router-dom";

const TermsAndConditions = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const userRegisterData = useSelector((state) => state.userRegister);
 //const { authState } = userRegister;
  const _userName = useSelector((state) => state.userName);
  const{ details} = useSelector((state) => state.userNumber);
  const { loading, otp } = useSelector((state) => state.userOtp);
  let saveOtp = [];
  

  const onAccept = () => {
   // e.preventDefault();
       
    // let userdata = {
    //     'email': details.email,
    //     "firstName": details.firstName,
    //     "middleName": details.middleName,
    //     "lastName":  details.lastName,
    //     "phone": details.phone,
    //     "countryCode": details.countryCode,
    //     "userName": details.userName,
    //     "password": details.password,
    //     "longitude": '31.434534534',
    //     'latitude': '75.234234234',
    //     'customerId': details.customerId,
    //     'profile': details.profile,}
    
    
      history.push("/auth/uploadlicence");
     //console.log(userdata)
    
    
  };

  useEffect(() => {
   
  }, []);

  return (
    <div class="wrap">
    <div class="choose-option">
        <div class="co-left">
            <figure class="content-img">
                <img src="../assets/images/png/content.png"/>
            </figure>
            <div class="back-btn">
                <figure class="backicon">
                  <a href="index.html"><img src="../assets/images/svg/back.svg"/></a> 
                </figure>
            </div>
        </div>
        <div class="co-right content-page">
            <div class="content-page">
                <h3 class="text-center">Terms and Conditions</h3>
                <p class="text-para text-center">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                    the
                    industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of
                    type
                    and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                    the
                    leap into electronic typesetting, remaining essentially unchanged. It was popularised in the
                    1960s
                    with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                    desktop
                    publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                <h4 class="query text-para">Why do we use it?</h4>
                <p class="text-para text-center">
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                    the
                    industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of
                    type
                    and scrambled it to make a type specimen book. It has survived not only five centuries, but also
                    the
                    leap into electronic typesetting, remaining essentially unchanged. It was popularised in the
                    1960s
                    with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                    desktop
                    publishing software like Aldus PageMaker including versions of Lorem Ipsum
                </p>
                <div class="btn-grp">
                  
                    <button class="btn btn-secondary active"onClick={() => onAccept() }    >Accept</button>
                    <button class="btn btn-secondary">Decline</button>
                </div>
            </div>
        </div>
    </div>
</div>

  );
};

export default TermsAndConditions;

