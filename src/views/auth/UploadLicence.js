import React, { useState, useEffect } from "react";
import { userName,saveNumber,userDocuments} from '../../services/authservice';
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useParams, useHistory } from "react-router-dom";
import ShowModal from "./ShowModal";


const UploadLicence = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const { document } = useSelector((state) => state.userDocument);
  const { authState } = useSelector((state) => state.userRegister);
 //const { authState } = userRegister;
  const _userName = useSelector((state) => state.userName);
  const{ details} = useSelector((state) => state.userNumber);
  const [documentone,setDocumentOne] = useState('')
  const [documenttwo,setDocumentTwo] = useState('')
  

  const onUpload = (e) => {
    e.preventDefault();
   let formDataOne = new FormData();
   formDataOne.append('documentOne', documentone);

   formDataOne.append('documentTwo', documenttwo);
  
  // let userdocuments 
//    for (var key of formDataOne.entries()) {

//     console.log(key[0] + ", " + key[1]);
//   }

   console.log( 'doc',formDataOne )
   dispatch(userDocuments(authState,formDataOne ))
    
     // history.push("/auth/accountdetail");
     //console.log(userdata)
  };

 const onContnue = () => {

   history.push("/main");
 }


  useEffect(() => {
     
    console.log("user token", authState);
   // console.log("user upload status", document.data['sucess']);
   
   }, [authState,document]);

  const setFileUploadOne = (e) => {
    setDocumentOne(e.target.files[0]);
    //setDocumentOne(e.target.files[0].name);
    }

    const setFileUploadTwo = (e) => {
        setDocumentTwo(e.target.files[0]);
       // setDocumentTwo(e.target.files[0].name);
        }

  useEffect(() => {
   
  }, []);

  return (
       <div>
        

    <div class="wrap">
    <div class="choose-option">
        <div class="co-left">
            <figure class="content-img">
                <img src="../assets/images/png/uploaddoc.png"/>
            </figure>
            <div class="back-btn">
                <figure class="backicon">
                    <a href="alllogin.html"><img src="../assets/images/svg/back.svg"/></a>
                </figure>
            </div>
        </div>
        <div class="co-right uploadlincence">
            <div class="upinner">
                <div class="upload-lincencetop">
                    <figure class="upload-lincense">
                        <img src="../assets/images/svg/upload.svg"/>
                    </figure>
                    <h5>Driving Licence & Passport</h5>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                        been the industry's standard dummy text ever since the 1500s, when a</p>
                </div>
                <form onSubmit={onUpload}>
                <div class="upload-lincencebottom">
                    <div class="upload-btnlicnce">
                        <button class="btn btn-darkblue" onSubmit={(e) => onUpload(e)}   >Upload</button>
                    </div>
                    <p>Select the Document From Gallery
                       <span class="d-block"> JPG , PNG , PDF</span></p>
                    <ul>
                        <li>
                            <div class="licence-uploadbx">
                                <input type="file"   class
                                id="documentOne"
                                name="documentOne"
                                onChange={(e) => setFileUploadOne(e)  }
                                required
                                />
                                <figure><img src="../assets/images/jpg/doc.jpg"/></figure>
                            <div class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></div>
                            </div>
                            
                        </li>
                        <li>
                            <div class="licence-uploadbx">
                                <input type="file"
                                 id="documentTwo"
                                 name="documentTwo"
                                 onChange={(e) => setFileUploadTwo(e)  }
                                 required
                                />
                                <figure><img src="../assets/images/jpg/doc.jpg"/></figure>
                            <div class="close-btn"><i class="fa fa-times" aria-hidden="true"></i></div>
                            </div>
                            
                        </li>
                    </ul>
                   
                    <div class="btn-outer">
                        <button  class="btn btn-primary" data-toggle="modal"
                        data-target="#register"
                        onClick={() => onContnue()  }
                        >Continue</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
  );
};

export default UploadLicence;

