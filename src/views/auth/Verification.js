import React, { useState, useEffect } from "react";
import { userName,saveNumber,userRegister} from '../../services/authservice';
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useParams, useHistory } from "react-router-dom";

const Verification = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const userRegisterData = useSelector((state) => state.userRegister);
 //const { authState } = userRegister;
  const _userName = useSelector((state) => state.userName);
  const{ details} = useSelector((state) => state.userNumber);
  const { loading, otp } = useSelector((state) => state.userOtp);
  let saveOtp = [];
  

  const onContinue = () => {
   // e.preventDefault();
       
    let userdata = {
        'email': details.email,
        "firstName": details.firstName,
        "middleName": details.middleName,
        "lastName":  details.lastName,
        "phone": details.phone,
        "countryCode": details.countryCode,
        "userName": details.userName,
        "password": details.password,
        "longitude": '31.434534534',
        'latitude': '75.234234234',
        'customerId': details.customerId,
        'profile': details.profile,}

        console.log( 'deatils are ',details[0]  )
    
    if (saveOtp.length != 0) {
      history.push("/auth/accountdetail");
     //console.log(userdata)
     dispatch(userRegister(details ))
     
    }
    
  };

  useEffect(() => {
    saveOtp = otp;
    console.log("user details", details);
    console.log("otp recieved", otp);
  }, [details, otp]);

  return (
    <div class="wrap">
      <div class="choose-option">
        <div class="co-left">
          <figure class="content-img">
            <img src="../assets/images/png/verification.png" />
          </figure>
          <div class="back-btn">
            <figure class="backicon">
              <a href="alllogin.html">
                <img src="../assets/images/svg/back.svg" />
              </a>
            </figure>
          </div>
        </div>
        <div class="co-right">
          <figure class="logo">
            <img src="../assets/images/svg/logo.svg" />
          </figure>
          <form class="auth-form">
            <div class="verification-content">
              <h5>Verifying Phone Number !</h5>
              <p>Please enter the verification code sent to </p>
              <p>{details.phone}</p>
            </div>
            {  loading != true ? 
            <ul class="otp-list">
              <input
                type="text"
                class="formcontrol"
                placeholder="1"
                defaultValue={otp[0]}
              />
              <input
                type="text"
                class="formcontrol"
                placeholder="2"
                defaultValue={otp[1]}
              />

              <input
                type="text"
                class="formcontrol"
                placeholder="3"
                defaultValue={otp[2]}
              />

              <input
                type="text"
                class="formcontrol"
                placeholder="4"
                defaultValue={otp[3]}
              />
            </ul> :  <text> LOADING..</text>}
          </form>
          <button class="btn btn-primary" onClick={() => onContinue()}>
            Continue
          </button>
          <div class="oroption">
            <p class="text-para">
              <a href="signup.html">Resend Code</a>
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Verification;

/**
 * details.email,
        details.firstname,
        details.middlename,
        details.lastname,
        details.username,
        details.phone,
        details.countryCode,
        details.password,
        details.profile,
        details.longitude,
        details.latitude,
        details.customerId,
 */