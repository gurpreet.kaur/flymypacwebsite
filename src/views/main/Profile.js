import React, { useEffect,useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import { getProfileData,updateAdminProfileData } from "../../services/profileService";

const Profile = () => {
  const dispatch = useDispatch();
  const userSignin = useSelector((state) => state.userSignin);
  const { authState } = userSignin;
  const profiledata = useSelector((state) => state.profileData);
  const adminProfile = profiledata.profile;

  const [file,setFile] = useState('');
  const [filename,setFileName] = useState('');
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [username, setUsername] = useState("");
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [middlename, setMiddlename] = useState("");
  const [phonenumber, setPhonenumber] = useState("");

  const setFileUpload = (e) => {
    setFile(e.target.files[0]);
    setFileName(e.target.files[0].name);
 
    }


    return(
        <div class="common-sec">
        <div class="conta-iner">
            <div class="common-sec__main">
             <div class="common-sec__l">
                 <div class="userimg-bx">
                     <figure><img src="images/png/user.png" alt=""/></figure>
                       <h3>Amit sharma <span>prelookstudio@gmail.com</span></h3>
                 </div>
                 <div class="list">
                     <a href="profilepage.html" class="active">Account</a>
                       <ul id="managepac-drop">
                           <li><a href="https://google.com" class="position-relative">Manage PACs
                            <img class="dropicon" src="images/png/dropicon.png" alt=""/>
                           </a>
                             <ul>
                               <li><a href="#" class="position-relative">On Going   <img class="dropicon dir-change" src="images/png/dropicon.png" alt=""/></a>
                                 <ul>
                                   <li><a href="ontheway.html" class="position-relative">On the Way  <img class="dropicon right-arrow" src="images/png/right-arrow.png" alt=""/></a></li>
                                   <li><a href="dispute.html" class="position-relative">Disputed  <img class="dropicon right-arrow" src="images/png/right-arrow.png" alt=""/></a></li>
                                   <li><a  href="ongoing_pack.html" class="position-relative">On Going <img class="dropicon right-arrow" src="images/png/right-arrow.png" alt=""/></a></li>

                                 </ul>
                               </li>
                               <li><a href="#" class="position-relative">Past <img class="dropicon dir-change" src="images/png/dropicon.png" alt=""/></a>
                                 <ul>
                                   <li><a href="completedpackage.html" class="position-relative">Completed  <img class="dropicon right-arrow" src="images/png/right-arrow.png" alt=""/></a></li>
                                   <li><a href="cancelled.html" class="position-relative">Cancelled <img class="dropicon right-arrow" src="images/png/right-arrow.png" alt=""/></a></li>
                                   <li><a href="dispute.html" class="position-relative">Disputed <img class="dropicon right-arrow" src="images/png/right-arrow.png" alt=""/></a></li>
                                 </ul>
                               </li>
                             </ul>
                           </li>
                         </ul>
                    
                     <a href="changepassword.html">Change Password</a>
                     <a href="mydocument.html">My Documents</a>
                     <a href="messagescreen.html">Messages</a>
                     <a href="rating.html">Rating</a>
                 </div>
              </div>
                <div class="common-sec__r">
                    <div class="bgcolor">
                     <div class="user-account">
                         <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                             <li class="nav-item"> <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-main" role="tab" aria-controls="pills-home" aria-selected="true">Profile</a></li>
                             <li class="nav-item"> <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-account" role="tab" aria-controls="pills-a" aria-selected="false">Bank Account</a></li>
                         </ul>
                         <div class="tab-content" id="pills-tabContent">
                             <div class="tab-pane fade show active" id="pills-main" role="tabpanel" aria-labelledby="pills-home-tab">
                                 <form action="">
                                     <div class="usreditimg">
                                         <figure class="figure"><img src="images/png/user.png" alt=""/></figure>
                                         <figure class="edit-icon">
                                             <img src="images/png/upload-icon.png" alt=""/>
                                             <input type="file" data
                                              onChange={(e) => setFileUpload(e)}
                                             />
                                         </figure>
                                     </div>
                                     <div class="row">
                                         <div class="col-md-6 input-layout">
                                             <label for="">First Name</label>
                                             <input type="text"
                                               value={firstname}
                                               onChange={(e) => {
                                                 setFirstname(e.target.value)
                                               }}
                                             placeholder="Amit"/>
                                         </div>
                                         <div class="col-md-6 input-layout">
                                             <label for="">Last Name</label>
                                             <input type="text"
                                             value={lastname}
                                               onChange={(e) => {
                                                setLastname(e.target.value)
                                              }}
                                             placeholder="Sharma"/>
                                         </div>
                                         <div class="col-md-6 input-layout">
                                             <label for="">Middle Name</label>
                                             <input type="text"
                                              value={middlename}
                                               onChange={(e) => {
                                                setMiddlename(e.target.value)
                                              }}
                                             placeholder="Sharma"/>
                                         </div>
                                         <div class="col-md-6 input-layout">
                                             <label for="">Email Address</label>
                                             <input type="text"
                                              value={email}
                                               onChange={(e) => {
                                                setEmail(e.target.value)
                                              }}
                                             placeholder="Amitsharma@gmail.com"/>
                                         </div>
                                         <div class="col-md-6 input-layout">
                                             <label for="">Phone Number</label>
                                             <input type="text"
                                              value={phonenumber}
                                               onChange={(e) => {
                                                setPhonenumber(e.target.value)
                                              }}
                                             placeholder="123456789"/>
                                         </div>
                                         <div class="col-md-6 input-layout">
                                             <label for="">User Name</label>
                                             <input type="text"
                                             disabled={true}
                                                defaultValue={username}
                                                value={username}
                                             placeholder="Amit234"/>
                                         </div>
                                     </div>
                                     <div class="btn-outer edit-btn">
                                         <a href="" class="btn btn-green">Edit</a>
                                     </div>
                                  
                                 </form>
                             </div>
                             <div class="tab-pane fade" id="pills-account" role="tabpanel" aria-labelledby="pills-profile-tab">
                                 <div class="usr-bankaccount col-md-8">
                                     <div class="btn-outer ">
                                         <a href="" class="btn btn-paypal">Paypal</a>
                                     </div>
                                     <p class="or">Or</p>
                             <form>
                                 <div class="input-layout ">
                                   <label for="">Bank Account</label>
                                   <input type="text"
                                     
                                   placeholder="Bank Name"/>
                                 </div>
                                 <div class="input-layout ">
                                     <input type="text"
                                       
                                     placeholder="Account Holder Name"/>
                                   </div>
                                   <div class="input-layout ">
                                     <input type="text"
                                       
                                     placeholder="Account Number"/>
                                   </div>
                                   <div class="input-layout ">
                                     <input type="text"
                                       
                                     placeholder="Account Type"/>
                                   </div>
                                   <div class="btn-outer edit-btn">
                                     <a href="" class="btn btn-green">Edit</a>
                                   </div>
                             </form>
                         
                                 </div>
                             </div>
                         <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">Three</div> 
                         </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    </div>
    )
}

export default Profile;